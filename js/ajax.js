(function($) {

$('.menu.nav li a, a.logo').unbind('click')
  Drupal.behaviors.ajaxify = {
    attach: function (context, settings) {
    	// if (!settings) settings = Drupal.settings.ajaxify;
    // $('.menu.nav li a, a.logo')	
	$('.menu.nav li a, a.logo').live('click', 	function(e) {
	        // makeAjax();
	  		var url = $(this).attr('href');
	  		// loadAjax(url, e)	
	  		// $.address.value(url.replace(/^#/, '').replace('http://'+document.domain,''));
	  		e.preventDefault()
	  		$.address.value(url.replace(/^#/, '').replace('http://'+document.domain,''));
	  		// console.log(context)  
	    });
    }
  };	
	

	function loadAjax(url, event) {
		$('body').addClass('loading');
		$.ajax({
			type : "POST",
			url : url,
		}).error(function() {
			$('body').removeClass('loading');
			triggerError();
		}).success(function(response_html) { 
			$('.page').html(response_html)
			title = $("h1", response_html);
			$('body').removeClass('loading');

			// if(title) {
			// 	 $.address.title(title.text()+ ' GetReal ')
			// }
			
	  })	
	}

	function triggerError(){
		alert('Something went wrong, please refresh the page')
	}



	  	$.address.init(function(e) {
	      console.log("init: ");
	      $.address.crawlable(true)
	  	})
	  	.change(function(e) {
				var uri = e.value;
				console.log(e);
				if (e.value != '/') {
					loadAjax(uri, e)
				}
		})   

})(jQuery);

// Drupal.behaviors.ajaxify = function(context, settings){
//     if (!settings) settings = Drupal.settings.ajaxify;
//     $("'.menu.nav li a, a.logo'", context).click(function() {
//         makeAjax();
//     });

//   	$.address.init(function(event) {
//       console.log("init: ");
//       $.address.crawlable(true)
//   	})
//   	.change(function(event) {
// 			var uri = event.value;
// 			console.log("change: ");
// 			if (event.value != '/') {
// 				loadAjax(uri, event)
// 			}
// 	})    
// };