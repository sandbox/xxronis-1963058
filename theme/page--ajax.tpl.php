
<div id="ajax-wrapper">
<?php if ($page['highlighted']): ?>
  <div class="highlighted hero-unit"><?php print render($page['highlighted']); ?></div>
<?php endif; ?>
<?php if ($breadcrumb): print $breadcrumb; endif;?>
<a id="main-content"></a>
<?php print render($title_prefix); ?>
<?php if ($title): ?>
  <h1 class="page-header"><?php print $title; ?></h1>
<?php endif; ?>
<?php print render($title_suffix); ?>
<?php print $messages; ?>
<?php if ($tabs): ?>
  <?php print render($tabs); ?>
<?php endif; ?>
<?php if ($page['help']): ?> 
  <div class="well"><?php print render($page['help']); ?></div>
<?php endif; ?>
<?php if ($action_links): ?>
  <ul class="action-links"><?php print render($action_links); ?></ul>
<?php endif; ?>
<?php print render($page['content']); ?>
<?php drupal_add_js(array('ajaxTitle' => array('title' => 'drupal_get_title()')), 'setting'); ?>
</div>